all: .venv app/node_modules

.venv:
	python -m venv .venv
	.venv/bin/pip install -U pip
	.venv/bin/pip install -U wheel
	.venv/bin/pip install -r requirements.txt

app/node_modules:
	cd app && npm install

clean:
	rm -rf .venv
	rm -rf app/node_modules
